import React, { useCallback } from 'react';
import { connect } from 'react-redux';
import { Row, Col } from 'antd';

import { StatsData } from '../../types/generated/midgard';
import { RootState } from '../../redux/store';
import { AssetDetailMap } from '../../redux/midgard/types';
import { StyledStatistic } from './StatisticsView.style';

type Props = {
  assets: AssetDetailMap;
  stats: StatsData;
};

const StatisticsView: React.FC<Props> = (props: Props): JSX.Element => {
  const { assets, stats } = props;
  const busdPrice = assets?.['BUSD-BAF']?.priceRune ?? '1';
  const price = Number(busdPrice);

  const getUSDValue = useCallback(
    (val: string) => {
      return (Number(val) / 1e8 / price).toFixed(2);
    },
    [price],
  );

  return (
    <Row gutter={[16, 16]}>
      <Col
        xs={{ span: 24 }}
        sm={{ span: 12 }}
        md={{ span: 8 }}
        lg={{ span: 8 }}
        xl={{ span: 4 }}
      >
        <StyledStatistic
          title="Total Staked"
          value={getUSDValue(stats?.totalStaked ?? '0')}
          prefix="$"
        />
      </Col>
      <Col
        xs={{ span: 24 }}
        sm={{ span: 12 }}
        md={{ span: 8 }}
        lg={{ span: 8 }}
        xl={{ span: 4 }}
      >
        <StyledStatistic
          title="Total Staked Tx"
          value={stats?.totalStakeTx ?? '0'}
        />
      </Col>
      <Col
        xs={{ span: 24 }}
        sm={{ span: 12 }}
        md={{ span: 8 }}
        lg={{ span: 8 }}
        xl={{ span: 4 }}
      >
        <StyledStatistic
          title="Total Earned"
          value={getUSDValue(stats?.totalEarned ?? '0')}
          prefix="$"
        />
      </Col>
      <Col
        xs={{ span: 24 }}
        sm={{ span: 12 }}
        md={{ span: 8 }}
        lg={{ span: 8 }}
        xl={{ span: 4 }}
      >
        <StyledStatistic title="Total Tx" value={stats?.totalTx ?? '0'} />
      </Col>
      <Col
        xs={{ span: 24 }}
        sm={{ span: 12 }}
        md={{ span: 8 }}
        lg={{ span: 8 }}
        xl={{ span: 4 }}
      >
        <StyledStatistic
          title="Total Depth"
          value={getUSDValue(stats?.totalDepth ?? '0')}
          prefix="$"
        />
      </Col>
      <Col
        xs={{ span: 24 }}
        sm={{ span: 12 }}
        md={{ span: 8 }}
        lg={{ span: 8 }}
        xl={{ span: 4 }}
      >
        <StyledStatistic title="Total Users" value={stats?.totalUsers} />
      </Col>
      <Col
        xs={{ span: 24 }}
        sm={{ span: 12 }}
        md={{ span: 8 }}
        lg={{ span: 8 }}
        xl={{ span: 4 }}
      >
        <StyledStatistic
          title="Total Volume"
          value={getUSDValue(stats?.totalVolume ?? '0')}
          prefix="$"
        />
      </Col>
      <Col
        xs={{ span: 24 }}
        sm={{ span: 12 }}
        md={{ span: 8 }}
        lg={{ span: 8 }}
        xl={{ span: 4 }}
      >
        <StyledStatistic
          title="24HR Volume"
          value={getUSDValue(stats?.totalVolume24hr ?? '0')}
          prefix="$"
        />
      </Col>
      <Col
        xs={{ span: 24 }}
        sm={{ span: 12 }}
        md={{ span: 8 }}
        lg={{ span: 8 }}
        xl={{ span: 4 }}
      >
        <StyledStatistic
          title="Total Asset Buys"
          value={stats?.totalAssetBuys ?? '0'}
        />
      </Col>
      <Col
        xs={{ span: 24 }}
        sm={{ span: 12 }}
        md={{ span: 8 }}
        lg={{ span: 8 }}
        xl={{ span: 4 }}
      >
        <StyledStatistic
          title="Total Asset Sells"
          value={stats?.totalAssetSells ?? '0'}
        />
      </Col>
      <Col
        xs={{ span: 24 }}
        sm={{ span: 12 }}
        md={{ span: 8 }}
        lg={{ span: 8 }}
        xl={{ span: 4 }}
      >
        <StyledStatistic
          title="Total Withdraw Tx"
          value={stats?.totalWithdrawTx ?? '0'}
        />
      </Col>
      <Col
        xs={{ span: 24 }}
        sm={{ span: 12 }}
        md={{ span: 8 }}
        lg={{ span: 8 }}
        xl={{ span: 4 }}
      >
        <StyledStatistic title="Pool Count" value={stats?.poolCount ?? '0'} />
      </Col>
      <Col
        xs={{ span: 24 }}
        sm={{ span: 12 }}
        md={{ span: 8 }}
        lg={{ span: 8 }}
        xl={{ span: 4 }}
      >
        <StyledStatistic
          title="Daily Active Users"
          value={stats?.dailyActiveUsers ?? '0'}
        />
      </Col>
      <Col
        xs={{ span: 24 }}
        sm={{ span: 12 }}
        md={{ span: 8 }}
        lg={{ span: 8 }}
        xl={{ span: 4 }}
      >
        <StyledStatistic title="Daily Tx" value={stats?.dailyTx ?? '0'} />
      </Col>
      <Col
        xs={{ span: 24 }}
        sm={{ span: 12 }}
        md={{ span: 8 }}
        lg={{ span: 8 }}
        xl={{ span: 4 }}
      >
        <StyledStatistic
          title="Monthly Active Users"
          value={stats?.monthlyActiveUsers}
        />
      </Col>
      <Col
        xs={{ span: 24 }}
        sm={{ span: 12 }}
        md={{ span: 8 }}
        lg={{ span: 8 }}
        xl={{ span: 4 }}
      >
        <StyledStatistic title="Monthly Tx" value={stats?.monthlyTx} />
      </Col>
    </Row>
  );
};

export default connect((state: RootState) => ({
  stats: state.Midgard.stats,
  assets: state.Midgard.assets,
}))(StatisticsView);
